from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('quotation', views.quotation, name='quotation'),
]
