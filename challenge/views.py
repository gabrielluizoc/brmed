from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.template import loader
import datetime
from datetime import date
import requests


def index(request):
    return render(request, 'challenge/base.html')


def quotation(request):
    last_five_days = [request.GET.get('date')]
    base = request.GET.get('base')
    rate = request.GET.get('rate')
    quote = []
    for i in range(4):
        last_five_days.append((date.fromisoformat(request.GET.get('date')) - datetime.timedelta(days=i+1)).strftime("%Y-%m-%d"))

    for i in range(5):
        req = requests.get("https://api.vatcomply.com/rates?base={}&date={}".format(base, last_five_days[i]))
        quote.append([datetime.datetime.strptime(last_five_days[i], '%Y-%m-%d').timestamp() * 1000,
                      req.json()['rates'][rate]])

    return JsonResponse(quote, safe=False)
