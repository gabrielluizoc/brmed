## Desafio BRMed

### Instalação do Docker
Para a execução do projeto basta com que se tenha o Docker
instalado, para isto é bem simples, no seguinte endereço
escolha seu Sistema Operacional e realize o passo a passo de instalação [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)

### Primeiro Passo
Com o Docker instalado em sua máquina crie uma imagem para
o projeto utilizando o seguinte comando 
`docker build -t brmed .` dentro da pasta raíz do projeto

### Segundo Passo
Após a criação da imagem Docker, agora vamos criar o nosso container
e para isto basta executar o seguinte comando em seu terminal 
`docker run -p 8000:8000 --name brmed -d brmed`

Após a execução destes comandos sua aplicação estará sendo executada no endereço
[http://127.0.0.1:8000/](http://127.0.0.1:8000/)
